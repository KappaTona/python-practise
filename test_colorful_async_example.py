#!/usr/bin/env python3
"""
Module file for invoking colorful_async_example hardcoded example
"""
from unittest.mock import MagicMock
import pytest
import colorful_async_example


@pytest.mark.parametrize("delay", [True, False])
def test_example_output(delay: bool):
    """
    invoking colorful random generation with `delay`.

    Parameters:
        delay: bool
            If True: enters a branch with mimiced delay
            If False: enters a branch without mimiced delay
    """
    print("Test Case 1: Invoke example_run")
    cyan, red, purple, green, no_color = colorful_async_example.example_run(
                                        delay)
    assert cyan is None, "cyan color is not returned"
    assert red is None, "red is not returned."
    assert purple is None, "purple is not returned."
    assert green is None, "green is not returned"
    assert no_color is None, "no color is not returned"


@pytest.mark.parametrize("custom_arg", [[9, 6, 3], [3, 2, 1]])
def test_example_chain(custom_arg):
    """
    invoking a colorful version of the chain.py example.
    """
    colorful_async_example.example_chain(custom_arg)


def test_example_queue():
    """
    Invoking Producer and Consumer example. Uses syncronized produce
    """
    colorful_async_example.execute_queue_example()


@pytest.mark.parametrize("producers_list", [[]])
def test_produce_task_list_as_list(producers_list: list):
    """
    Invoking Producer and Consumer Queue example.

    Additional Description:
        `producers_list` should be filled with `asyncio.create_task`
        We use the expectations that the `asyncio.create_task` parameter
        funtion returns True or RuntimeException when the given task is
        consumed.
    Expecting:
        - producer_list should not be empty after `execute_queue_example`
        - producer_list `str` items not contain the word: false
    """
    colorful_async_example.execute_queue_example(producers_list)
    assert len(producers_list), "producers list is empty"
    for should_be_true in producers_list:
        assert "false" not in should_be_true.lower(), f"""
        all item should be `True` but {should_be_true} is not"
        """


@pytest.mark.parametrize("producers_list", [[]])
def test_produce_task_list_has_exception(producers_list: list):
    """
    Invoking `execute_queue_example` when make item has a side effeft
    Raising RuntimeError for simulation purpose
    """
    async def make_item_side_effect():
        raise RuntimeError("Simulate Runtime Error")

    mocked_make_item = colorful_async_example
    mocked_make_item.make_item = MagicMock(side_effect=make_item_side_effect)
    colorful_async_example.execute_queue_example(producers_list)
