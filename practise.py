from dataclasses import dataclass


@dataclass
class ParameterPack:
    file_name: str
    git_path: str
    project_container: str
    leave_code_unformatted: bool
    mark_start: str
    mark_stop: str


def extracting_justification(pp: ParameterPack):
    print(pp)


pp = dict({
    "file_name": "this.txt",
    "git_path": "gitlab.com",
    "project_container": "default",
    "leave_code_unformatted": True,
    "mark_start": "GCOVR_EXCL_START",
    "mark_stop": "GCOVR_EXCL_STOP",
    "xd": "GCOVR_EXCL_STOP",
    "xxx": "GCOVR_EXCL_STOP",
    })
del pp["xd"]
del pp["xxx"]
xd = ParameterPack(*(pp.items()))

extracting_justification(pp)
