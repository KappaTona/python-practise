#!/usr/bin/env python3
"""
This module covers the example implementation found here:
https://realpython.com/async-io-python/#the-rules-of-async-io
with some modification
"""
import asyncio
import random
import time
import os


ansi_colors = dict([
    ("cyan", "\033[36m"),
    ("green", "\033[32m"),
    ("red", "\033[91m"),
    ("purple", "\033[35m"),
    ("no", "\033[0m"),
])
"""
TERM_COLOR for colored message output.
Used via `make_random_colorized` function and more.
"""


async def make_random_colorized(color, threshold: int = 6,
                                mimic_delay: bool = False) -> int:
    """
    Makes a random number and prints out colorized. Delayes the execution
    on demand to look the execution async like.

    Params:

    color:
        Incoming paramter that holds the color name and code.

    threshold: int
        The minimum required random number

    mimic_delay: bool = False
        mimics delay to see that the function is async
    """
    print(color[1] + f"make_random_colorized({color[0]}, {threshold}).")
    i = random.randint(0, 10)
    while i <= threshold:
        print(color[1]
              + f"make_random_colorized({color[0]}) == {i} too low; retrying")
        if mimic_delay:
            await asyncio.sleep(1-random.randint(0, 1))
        i = random.randint(0, 10)

    print(color[1] + f"--> Finished: make_random_colorized({color[0]}) == {i}"
          + ansi_colors["no"])      # clears terminal font color


def example_run(delay: bool = False):
    """
    Provides an example how to execute the `make_random_colorized` function.

    Parameters:

    delay: bool = False
        If True: uses the hardcoded delay for simulation
        If False: executes without delay
    """
    async def main():
        return await asyncio.gather(*(
            make_random_colorized((color_name, color), 7, delay)
            for color_name, color in ansi_colors.items()))

    random.seed(777)
    print()
    return asyncio.run(main())


async def part1(n: int) -> str:
    """
    cyan colorized print output.
    this function is a `part1` of a future chain

    Parameters:
        n: int
            represents the incoming number from main
            task
    """
    i = random.randint(1, 2)
    print(ansi_colors["cyan"] + f"cyan_part({n}) sleeping for {i} seconds.")
    await asyncio.sleep(i)
    result = f"result{n}-1"
    print(ansi_colors["cyan"] + f"Returning cyan_part({n}) == {result}.")
    return result


async def part2(n: int, arg: str) -> str:
    """
    green colorized print output.
    this function is a `part2` of a future `chain`

    Parameters:
        n: int
            represents the incoming number from main
            task

        arg: str
            A `string` received from `part1` containing the result.

    Return:
        result: str
            received sub result from part1 and the statement fromp
            part2 given back to main.
    """
    i = random.randint(0, 3)
    print(ansi_colors["green"]
          + f"green_part{n, arg} sleping for {i} seconds.")
    await asyncio.sleep(i)
    result = f"result{n}-2 derived from {arg}"
    print(ansi_colors["green"] + f"Returning green_part{n, arg} == {result}.")
    return result


async def chain(n: int) -> None:
    """
    a `chain` function to collect part1 and part2 sub stasks futures.

    Paramters:
        int: n
            represents the incoming number from main
            task. Forwards it to part1, part2 awaitable objects.

    Returns:
        None

    """
    start = time.perf_counter()
    cyan_part = await part1(n)
    green_part = await part2(n, cyan_part)
    end = time.perf_counter() - start
    print(f"--> result{n} => {green_part} took {end:0.2f} seconds).")


def example_chain(custom_args: list):
    """
    Furthermore local `main` function collects awaitable tasks
    and gathers their future into one iterable object
    so the function can be passed to `asyncio.run`.

    Parameters:
        custom_args: list
            A list containing numbers
    """
    async def main(*args):
        await asyncio.gather(*(chain(n) for n in args))

    random.seed(774)
    args = custom_args
    start = time.perf_counter()
    asyncio.run(main(*args))
    end = time.perf_counter() - start
    print(ansi_colors["red"] + f"--> Finished in {end:0.2f} seconds."
          + ansi_colors["no"])


async def make_item(size: int = 5) -> str:
    """
    `make_item` is a forward call and return to os.urandom(`size`).hex()
    """
    return os.urandom(size).hex()


async def random_sleep(caller=None):
    """
    Function that mimics delay to represent async IO.

    Parameters:
        caller=None:
            caller hints the name of either producer or consumer
    """
    i = random.randint(0, 3)
    if caller:
        print(ansi_colors["purple"] +
              f"{caller} sleeping for {i} seconds." + ansi_colors["no"])
    await asyncio.sleep(i)


async def produce(name: str, transmitter: asyncio.Queue):
    """
    Function that is the Produce part from
    the Consume And Produce design pattern.

    Parameters:
        name: str
        is the producer's name

        transmitter: asyncio.Queue
        is the connection to the unknown consumer
    """
    n = random.randint(0, 10)

    async def synchronous_single_producer(n: int):
        """
        Function that uses syncronized single producer pattern.
        Side-note: We don't want to re-raise the exception if happens.
        In the end we will have a collected result list
        containing success rate.
        The programflow should not be disturbed with exception
        in this particular case.

        Parameters:
            n: int
            Is the number of the iteration

        Returns:
            True: If everything executed without RuntimeException
            RuntimeException: otherwise
        """
        try:
            post_fix = "st" if n == 1 else "nd" if n == 2 else "th"
            print(ansi_colors["cyan"] +
                  f"{n}{post_fix} item is under producing..."
                  + ansi_colors["no"])
            await random_sleep(caller=f"Producer {name}")
            item = await make_item()
            produce_start = time.perf_counter()
            await transmitter.put((item, produce_start))
            print(ansi_colors["cyan"]
                  + f"Producer {name} put item: <{item}> in queue"
                  + ansi_colors["no"])
            return True
        except RuntimeError as e:
            print(ansi_colors["red"] +
                  f"Unexpected error: {e}" +
                  ansi_colors["no"])
            return e

    return [await synchronous_single_producer(i) for i in range(n)]


async def consume(name: str, transmitter: asyncio.Queue):
    """
    Function that is the Consume part
    from the Consume And Produce design pattern.
    The `while True` receives items from `produce`

    Parameters:
        name: str
        is the consumer's name

        transmitter: asyncio.Queue
        is the connection to the unknown producer
    """
    while True:
        await random_sleep(caller=f"Consumer {name}")
        item, produce_start = await transmitter.get()
        now = time.perf_counter()
        print(ansi_colors["green"] +
              f"Consumer {name} got element: <{item}>" +
              f" in {now-produce_start:0.5f} seconds" + ansi_colors["no"])
        transmitter.task_done()


def execute_queue_example(producers_result=None):
    """
    Function that collets and wraps up the Consume and Produce example.
    """
    async def transmitter_main(producers_result=None):
        """
        Function that will be passed to `asyncio.run`
        Creates named prodcuers and consumers with `asyncio.create_task`
        Gathers and `awaits` the `producers` tasks
        Joins both `producers` and `consumers`
        Cancels the `consumers` so they no longer wait in vanity.
        """
        transmitter = asyncio.Queue()
        producers = [asyncio.create_task(produce(name, transmitter))
                     for name in ["alfa", "omega"]]
        consumers = [asyncio.create_task(consume(name, transmitter))
                     for name in ["kappa", "teta", "lule"]]
        await asyncio.gather(*producers)

        # await producers, consumers
        await transmitter.join()

        for c in consumers:
            c.cancel()

        if producers_result is not None:
            for v in producers:
                producers_result.append(str(v))

    random.seed(747)
    start = time.perf_counter()
    asyncio.run(transmitter_main(producers_result))
    elapsed = time.perf_counter() - start
    print(ansi_colors["red"] +
          f"--> Program Finished in {elapsed:0.5f} seconds."
          + ansi_colors["no"])
