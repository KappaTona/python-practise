"""
Module for parsing C++ source files.
Searching for specific EXCLUSION MARKERS.
Generating markdown file.

Story:
    My experience with C++ code coverage in AUTOSAR led me to observe:
    That 100% on branch/decision coverage is impossible on legacy codebase.
    https://gcovr.com/en/stable/faq.html
    #why-does-c-code-have-so-many-uncovered-branches

Solution:
    Using `exclusion markers` with Template description.
    The module utilize the exclusion markers default: GCOVR_EXCL_{START,STOP}
    for finding segments to copy and paste into a markdown file for easy
    tracking.
"""
import re   # re.sub
import os   # os.remove
import argparse     # argprser.ArgumentParser
from dataclasses import dataclass, asdict


def split_at_project_name(file_path, project_container):
    """
    takes an absolute file_path paramter
    splits it at `ContainerName` set in `CMakeLists.txt` for the moment.
    It is `pretty` function for slicing paths.

    Example:
        file_path is: /opt/simulated-env/vendor/...../
        SpecificServerInterface/SpecificServerImpl.cpp
    Return:
        SpecificServerInterface/SpecificServerImpl.cpp
    """
    need = project_container + file_path.split(project_container)[1]
    print(need)
    return need


def remove_not_needed_empty_characters(line, leave_code_unformatted):
    """
    Takes a line, read from a source file
    removes indentations from comments

    Example:
        line is: \t\t\t Function: void....
    Returns:
        line without tabs or 4 spaces from the begning of the line
    """
    if not leave_code_unformatted:
        line = re.sub("[\t]*", "", line)
        line = re.sub("    ", "", line)
    else:
        if line.find("//") != -1:
            line = re.sub("[\t]*", "", line)
            line = re.sub("[    ]*", "", line)
    return line


@dataclass
class ArgumentPack:
    """
    Dataclass for parsing arguments from CLI.

    Data:
        file_name: str
        the (source) filename for where the exclusion markers found.
        Assuming absolute path for the moment.

        git_path: str
        Is the permanent link for the git master blob so the markdown
        links are clickable.

        project_container: str
        this variable should be removed was internal only.

        leave_code_unformatted: bool
        On legacy codebase sometimes there are
        no clean/clear defined code formatting configs.
        and lines start at row position 120 with 8 tabs.
        this setting removes the tabs from the markdown.

        mark_start: str
        mark_stop: str
        Both for the tool markers that can be customized on demand.
        https://gcovr.com/en/stable/guide/exclusion-markers.html
    """
    git_path: str
    project_container: str
    leave_code_unformatted: bool
    mark_start: str
    mark_stop: str
    justified_sources: str
    markdown: str


def extracting_justification(current_file, configs_options: ArgumentPack):
    """
    Extracts justifications from given file_name
    Creates quick summary and detailed description.
    - Quick Summary: just line numbers to the source files
    - Details: line nubmer + function name + justification reason

    Example:
        - Quick Summary:
        [Justification line number: 110]
        (https://path-to-git/blob/
        master/
        SpecificServerInterface/SpecificServerImpl.cpp#L110)
        - Details:
        [samelink](as quick summary)
        ```
        Function: void foo(bar)
        Branch Coverage (optional line)
        Justification Text...
        copy-paste code parts between the exclusion markers

    Return:
        - Quick Summary with markdown style link to the git repo
        and the quick summary string block
        - Details string pack
    """
    # string for details section
    details_section = ""
    # a bool flag to sign where the block begins
    append_from_here = False
    # string for quick summary section
    quick_summary = ""
    # count justification as we read through the file
    justification_count = 0
    with open(current_file, mode='rt',
              encoding='utf-8') as myfile:
        stripped_file_name = split_at_project_name(
            current_file, configs_options.project_container)
        quick_summary += f"Justifications for: {stripped_file_name}\n"
        details_section += f"Justifications for: {stripped_file_name}\n\n"

        # counting line numbers as we go on
        line_number = 0

        for line in myfile:
            line_number += 1
            # if case-insensitive match,
            if line.find(configs_options.mark_start) != -1:
                justification_count += 1
                markdown_link_to_line = "[Justification line number:"
                markdown_link_to_line += f"{line_number} ]("
                markdown_link_to_line += f"{configs_options.git_path}"
                markdown_link_to_line += f"{stripped_file_name}"
                markdown_link_to_line += f"#L{line_number})"
                markdown_link_to_line += "\n"

                quick_summary += "\t- "
                quick_summary += markdown_link_to_line
                details_section += markdown_link_to_line
                # code block begin
                details_section += "\n```\n"
                append_from_here = True

            if line.find(configs_options.mark_stop) != -1:
                append_from_here = False
                # code block end
                details_section += "```\n"

            if append_from_here:
                if not configs_options.leave_code_unformatted:
                    line = remove_not_needed_empty_characters(
                        line, configs_options.leave_code_unformatted)

                details_section += line

        quick_summary += "\n\njustification count: " + str(justification_count)
        quick_summary += "\n\n---\n\n"
        return quick_summary, details_section


def make_markdown(quick_summaries, details_section, markdown):
    """
    Makes a markdown file with quick summary section and details section
    Read the def extracting_justification(file_name) function for an example

    recreates jusstification-map.md

    Will raise: FileNotFoundError on clean build.
    """
    try:
        os.remove(markdown)
    except FileNotFoundError:
        pass

    with open(markdown, mode='a+', encoding='utf-8') as output:
        for qsi in quick_summaries:
            output.write(qsi)

        output.write("\n\nDetails below\n\n")

        for res in details_section:
            output.write(res)


def read_file_list(file_list="justified_files.txt"):
    """
    Reads given file list and then returns it

    Format is:
        Have a txt file with absolut paths listed
        separated by new lines

    Example:
        /opt/simulated-env/vendor/..../project/src/folder/foo.cpp
        /opt/simulated-env/vendor/..../project/src/folder/bar.cpp
    """
    result = []
    with open(file_list, mode="r", encoding="utf-8") as files:
        for file in files:
            result.append(file.rstrip('\n'))

    return result


def parse_arguments():
    """
    A standalone function for parsing CLI arguments and preparing the module.

    Returns:
        An `ArgumentPack` with settings provided.
    """
    main_description_text = """Justification markdown generator
    from given source files. The basic idea is to copy and paste code sections
    from the source files that has code
    coverage exclusion markers for example:
    GCOVR_EXCL_START [-b] and GCOVR_EXCL_STOP [-e] markers """

    git_path_help = """should contain the link for git repositroy
    ending with slash. For example: https://"""
    file_names_txt_help = """should contain the absolute paths to justified
    files separated by an end of line character.
    example can be seen in justified_files.txt """
    markdown_output_help = """could contain the path to the output file
    the default is to write the file next to this program """
    exclusion_start_text = """should contain the exclusion start mark.
    Defaults to: GCOVR_EXCL_START"""
    exclusion_stop_text = """should contain the exclusion stop mark.
    Defaults to: GCOVR_EXCL_STOP"""
    project_container_text = """should contain the project name
    usually equals to the CONTAINER_NAME example: SpecificServerInterface"""
    leave_code_unformatted = """True or False
    -> remove tabs from justification parts"""

    parser = argparse.ArgumentParser(description=main_description_text)
    parser.add_argument('-p', '--git_path', nargs='?', help=git_path_help,
                        type=str)
    parser.add_argument('-i', '--input_file', nargs='?',
                        help=file_names_txt_help, type=str,
                        default="justified_files.txt")
    parser.add_argument('-o', '--output_file', nargs='?',
                        help=markdown_output_help,
                        type=str, default="justification_map.md")
    parser.add_argument('-b', '--exclusion_start_mark', nargs='?',
                        help=exclusion_start_text,
                        type=str, default="GCOVR_EXCL_START")
    parser.add_argument('-e', '--exclusion_stop_mark', nargs='?',
                        help=exclusion_stop_text,
                        type=str, default="GCOVR_EXCL_STOP")
    parser.add_argument('-c', '--project_container', nargs='?',
                        help=project_container_text, type=str,
                        default="SpecificServerInterface")
    parser.add_argument('-l', '--leave_code_unformatted', type=bool,
                        default=False, help=leave_code_unformatted)

    args = parser.parse_args()
    return ArgumentPack(args.git_path,
                        args.project_container,
                        args.leave_code_unformatted,
                        args.exclusion_start_mark,
                        args.exclusion_stop_mark,
                        args.input_file,
                        args.output_file,)


def generate_markdown(argument_map):
    """
    Generates the markdown file.
    Receives the arguments/config settings from CLI.

    Parameters:
    arguemnt_map:
        contains config options `ArgumentPack` that are forwarded to
        `extracting_justification`
        """
    file_names = read_file_list(argument_map.justified_sources)
    summaries = []
    sections = []
    for file in file_names:
        summary, section = extracting_justification(file, argument_map)

        summaries.append(summary)
        sections.append(section)

    make_markdown(summaries, sections, argument_map.markdown)


if "__main__" == __name__:
    rv = parse_arguments()
    print("Options:")
    for i, j in asdict(rv).items():
        print(i, ": ", j)

    generate_markdown(rv)
