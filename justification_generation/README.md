# TODO
- furthermore provide options for project name

# What is this
A python script that reads given files
collect all parts with `GCOVR_EXCL_{START/STOP}`
and copies them into a markdown file

# HOW TO
- have a file list where justifications are present put them into the `justified_files.txt` with new line separation 
- just run the python script


Result should be a `justification-map.md` file which points to the master branch source files.


# Example

Input:

`Justified_files.txt`

Output:

Example of quick summary:

`	- [Justification line number: 180](https://gitlab.com/KappaTona/pyton-practise/`

Example of deatiled summary:

[Justification line number: 180](x)
```
TODO
```

[click For a full example ](x)

# Example usage as from shell

```
custom_justification_generation()
{
    echo "***INFO*** custom justification map generation is enabled"

    GIT_REPO="hard TODO"
    MARKDOWN="YEP"
    NAME="xd"

    python3 just-generate.py -p $GIT_REPO -o $MARKDOWN -c $NAME
}
```

# Example usage from CMake

- mkdir build
- cd build
- cmake ..
- make generate-markdown

the result will be located: `build/justification-map.md`

# Python Help Output
![help](help.png)
