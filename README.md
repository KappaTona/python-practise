# Sources:

- the examples taken from here: https://realpython.com/async-io-python/#the-rules-of-async-io
- the gitlab yaml taken from here: https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Python.gitlab-ci.yml
- TODO: https://realpython.com/introduction-to-python-generators/


# Self notes:

- `pytest -s` for seeing print options on test success as well
- `pylint` for static analysis
- `pytest --cov` for CLI output
- `pytest -s --cov --cov-report=html` for htmloutput
- `pytest -s --cov --cov-report=html --cov-report=term` for both

# TODO list

- TODO: re-add pages step from yaml file

Changed async queue example:
- return generated list which contains:
    - True if item is produced 
    - RuntimeException
        - TODO: use mock during pytest to force exception
- replaced itertools.repeat with range(n)
    - it is good to see versatile options on example but I felt that itertools.repeat(None, n) is here for mention sake only and I agree with it but I changed the example anyway.



# Notes

- While queues are often used in threaded programs because of the thread-safety of queue.Queue(), you shouldn’t need to concern yourself with thread safety when it comes to async IO. (The exception is when you’re combining the two, but that isn’t done in this tutorial.)

- One use-case for queues (as is the case here) is for the queue to act as a transmitter for producers and consumers that aren’t otherwise directly chained or associated with each other.

- example for keyword argument parsing:
```
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--nprod", type=int, default=5)
    parser.add_argument("-c", "--ncon", type=int, default=10)
    ns = parser.parse_args()
    start = time.perf_counter()
    asyncio.run(main(**ns.__dict__))
    elapsed = time.perf_counter() - start
```

- Generator functions are, as it so happens, the foundation of async IO (regardless of whether you declare coroutines with `async def` rather than the older `@asyncio.coroutine wrapper)`. Technically, `await` is more closely analogous to `yield from` than it is to `yield`. (But remember that `yield from x()` is just syntactic sugar to replace `for i in x(): yield i`.)

- Pytest:
    - `@pytest.mark.skip(reason="tmp, debug last test case only")`
    - `@pytest.mark.parametrize("custom_arg", [[9, 6, 3], [3, 2, 1]])`

